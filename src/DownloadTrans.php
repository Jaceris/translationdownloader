<?php

namespace idfortysix\translationdownloader;

use Illuminate\Console\Command;
use Storage;
use Carbon\Carbon;

class DownloadTrans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads json translations from S3 / OVH object storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		// Pasiimam lang json failus is OVH object storage
        $files = Storage::disk('translations')->files("/json");
		
		// 
		foreach ($files as $file)
		{
			// gaunam 2 raidziu language koda is file name
			$lang = explode('/', preg_split('/[-]/', $file)[0])[1];

			// Ikeliam lang.json faila i projekta
            file_put_contents(resource_path("lang/$lang.json"), Storage::disk('translations')->get($file));
		}
        
        // 1 day + 10min (extra time) 
        $this->info("SECONDCHECK:".(time() + 3600*24 + 600));
    }
}
