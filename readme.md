## Custom komandos

Atsiuncia lang.json failus is OVH object storage ir iraso i resources lang kataloga projekte. 
```
./artisan translations:download 
```

## Install
```
./composer.phar require id-forty-six/translationdownloader
```
Prie service provideriu prideti:
```
idfortysix\translationdownloader\TranslationServiceProvider::class
```
I config->filesystems reikia prideti:
```
'translations' => [
        'driver'     => 'ovh',
        'server'     => env('TRANS_ENDPOINT'),
        'user'       => env('TRANS_USERNAME'),
        'pass'       => env('TRANS_PASSWORD'),
        'region'     => env('TRANS_REGION'),
        'tenantName' => env('TRANS_TENANT_NAME'),
        'container'  => env('TRANS_TRANS_CONTAINER', 'translationmanager'),
        'projectId'  => env('TRANS_TENANT_ID'),
],
```
or
```
'translations' => [
        'driver'     => 's3',
        'key'        => env('TRANS_KEY'),
        'secret'     => env('TRANS_SECRET'),
        'region'     => env('TRANS_REGION'),
        'endpoint'   => env('TRANS_ENDPOINT'),
        'bucket'     => env('TRANS_BUCKET'),
],
```
